using System.Drawing;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class storeElementUI : MonoBehaviour
{
    public ShopList shopList;
    public ShopElement shopElement;
    public Image sprite;
    public TextMeshProUGUI price;
    public TextMeshProUGUI TextName;
    public GameObject ispurchased;

    private void Start()
    {
    }

    public void init()
    {
        sprite.sprite = shopElement.sprite;
        price.text = shopElement.price.ToString();
        TextName.text = shopElement.ElementName.ToString();
        ispurchased.SetActive(shopElement.purchased);
    }

    public void clicked()
    {
       var Coins = PlayerPrefs.GetInt("Coins", 0);
        if (Coins >= shopElement.price)
        {
            shopElement.purchased = true;
            ispurchased.SetActive(shopElement.purchased);
         Coins-=shopElement.price;
            PlayerPrefs.SetInt("Coins", Coins);
 
          

        
        }
        if (shopElement.purchased)
        {
            if (shopElement.elementType == Element.Bows)
                shopList.usedBows = shopElement;
            if (shopElement.elementType == Element.Arrows)
                shopList.usedArrows = shopElement;
        }
    }
}
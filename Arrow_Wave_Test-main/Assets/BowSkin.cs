using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowSkin : MonoBehaviour
{
   public ShopList ShopList;
    public SkinnedMeshRenderer SkinnedMeshRenderer;
    [ContextMenu("bows")]
    void Start()
    {
        
        if (ShopList.usedBows.Material != null)
        {
           
          
          for (int i = 0;i < SkinnedMeshRenderer.sharedMaterials.Length;i++)
            {
                SkinnedMeshRenderer.sharedMaterials[i]= ShopList.usedBows.Material;
            }
            SkinnedMeshRenderer.material = ShopList.usedBows.Material;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

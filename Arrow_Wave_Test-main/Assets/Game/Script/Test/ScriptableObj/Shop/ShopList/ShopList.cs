using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
[CreateAssetMenu(fileName = "ShopList", menuName = "Test/Shop/ShopList")]
public class ShopList : ScriptableObject
{
    public ShopElement usedBows;
    public ShopElement usedArrows;
    public List<ShopElement> items;
    private string savePath;
    private void OnEnable()
    {
   
        savePath = Application.persistentDataPath + "/shopList.json";

        LoadData();
    }

    private void OnDisable()
    {
      
        SaveData();
    }

    private void SaveData()
    {
   
        string jsonData = JsonUtility.ToJson(this);

        
        File.WriteAllText(savePath, jsonData);
    }

    private void LoadData()
    {

        if (File.Exists(savePath))
        {
            string jsonData = File.ReadAllText(savePath);

          
            JsonUtility.FromJsonOverwrite(jsonData, this);
        }
        else
        {
            Debug.LogWarning("Save file not found");
          
        }
    }
}

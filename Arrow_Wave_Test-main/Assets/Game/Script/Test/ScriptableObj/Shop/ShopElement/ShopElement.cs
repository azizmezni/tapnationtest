using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ShopElement", menuName = "Test/Shop/ShopElement")]
public class ShopElement : ScriptableObject
{
    public string ElementName;
    public Element elementType;
    public Sprite sprite;
    public int price;
    public GameObject Prefab; // to instatiate if we want , no use now
    public Material Material; // to change material only 
    public bool purchased;

}
public enum Element { Bows,Arrows}
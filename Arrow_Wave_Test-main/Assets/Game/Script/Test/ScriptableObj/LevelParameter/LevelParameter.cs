using UnityEngine;

[CreateAssetMenu(fileName = "LevelParameter", menuName = "Test/LevelParameter")]
public class LevelParameter : ScriptableObject
{
    public int coins;
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreInit : MonoBehaviour
{
 public ShopList ShopList;
    public Transform Bows;
    public Transform Arrows;
    public GameObject refrence;
    private void Start()
    {
        foreach (var item in ShopList.items)
        {
            GameObject uiElement = Instantiate(refrence, item.elementType == Element.Bows ? Bows:Arrows) ;
           
          var storeitem=  uiElement.GetComponent<storeElementUI>();
            storeitem.shopElement = item;
            storeitem.init();


        }
    }
}
